import time
import PLCtime
import PLCdata

import SUT
import integrationtest

d = PLCdata.data
sut = SUT.SUT()
it = integrationtest.IntegrationTest()

# Main loop = OB1
while True:

    # Normal program (Subject Under Test)
    sut.call()

    # Integration test call
    it.call()

    # Advance the virtual PLC time and slow down our program
    PLCtime.tick()
    time.sleep(1/20.0)


# Integration test:
# state machine, sequential
# Each state is one test
# At state change, create a rising edge "test init" to reset all items in this test

# A test 
# - first sets up the sut by setting DIs and acknowledgedes, or other stuff
# - triggers an action on the sut by setting a DI or an other internal dataitem
# - monitors behaviour by several rulesm eg.
#       - dataitem is always true/false
#        - dataitem is always/never a certain value
# The monitoring is done by seperate FBs, that are fed the "test init" to reset their internal state
# Each monitoring block has an output "passed" that is true by default, and false as soon as the rule is violated. The violated state is kept until the "test init" pulse is received
# The monitoring blocks can have delay times up front or deactivate after a ceratin time. This should be done by internal TONs etc. The times are fed into the block from outside
# Each test is running for a certain time. When time is up, the results are checked.



