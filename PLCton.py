
class Ton():
    """ Emulation of IEC TON timer
    """
    def __init__(self):
        self.startcond_HIS = False

        self.ET = 0
        self.Q = False
        self.state = "off"

    def IN(self, startcond, time):
        if startcond and not self.startcond_HIS:
            # start
            self.ET = time
            self.Q = False
            self.state = "running"
        elif startcond and self.state == "running":
            # time is running
            self.ET -= 1
            if self.ET <= 1:
                # time is up
                self.Q = True
                self.state = "elapsed"
        elif startcond and self.state == "elapsed":
           # Elapsed and output high, no change
           pass
        else:
            self.Q = False
            self.state = "off"

        # History
        self.startcond_HIS = startcond


        return self.Q