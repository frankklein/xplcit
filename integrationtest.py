import PLCdata
import PLCtime
import PLCton
import teststage1

stage1 = teststage1.Teststage1()

class IntegrationTest():
    def __init__(self):
        self.teststage = 0
        self.teststage_HIS = 0
        self.teststage_finished = False
        self.teststage_startpulse = False

        self.integrationTestFinished = False

    def call(self):
        if self.integrationTestFinished:
            return
        curTime = PLCtime.cycletime
        print("*************************************************")
        print("Current PLC time: {}".format(curTime))
        print()

        # init stage state
        if self.teststage <= 0:
            self.teststage = 1

        if self.teststage_startpulse:
            self.teststage_startpulse = False

        if self.teststage_finished:
            # start the next stage
            self.teststage_finished = False
            self.teststage += 1

        if self.teststage != self.teststage_HIS:
            self.teststage_startpulse = True

        ## 
        ## STAGE 1
        ##
        if self.teststage == 1:
            self.teststage_finished = stage1.call(self.teststage_startpulse)

            if self.teststage_finished:
                # examine results
                if False in stage1.testaspectpassed.values():
                    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                    print("STAGE {} failed!".format(self.teststage))
                    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

        ## 
        ## DEFAULT STAGE -> integration test finished
        ##
        else:
            self.integrationTestFinished = True

            print ("##################################################")
            print ("##################################################")
            print ("FINISHED")
            print ("##################################################")
            print ("##################################################")    

        self.teststage_HIS = self.teststage