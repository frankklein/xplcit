from PLCdata import data


class SUT():

    def __init__(self):
        self.counter = 0

    def call(self):
        self.counter += 1

        if self.counter > 10:
            data["SUToutput"] = True
        elif self.counter > 500:
            data["SUToutput"] = False

