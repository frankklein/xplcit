import PLCton

class AssertAfterTrue():
    def __init__(self):
        self.passed = False
        self.tonDelay = PLCton.Ton()
    
    def __call__(self, IN, after, startup):
        if startup:
            self.passed = True

        self.tonDelay.IN(not startup, after)

        if self.tonDelay.Q:
            if not IN:
                self.passed = False
        
        return self.passed
    
class AssertAfterFalse():
    def __init__(self):
        self.passed = False
        self.tonDelay = PLCton.Ton()

    def __call__(self, IN, after, startup):
        if startup:
            self.passed = True

        self.tonDelay.IN(not startup, after)

        if self.tonDelay.Q:
            if IN:
                self.passed = False
        
        return self.passed
