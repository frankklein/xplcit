

class AssertAlwaysTrue():
    def __init__(self):
        self.passed = False
    
    def __call__(self, IN, startup):
        if startup:
            self.passed = True
        
        if not IN:
            self.passed = False
        
        return self.passed
    
class AssertAlwaysFalse():
    def __init__(self):
        self.passed = False

    def __call__(self, IN, startup):
        if startup:
            self.passed = True
        
        if IN:
            self.passed = False
        
        return self.passed
