import PLCdata
import PLCtime
import PLCton

import AssertAlways
import AssertAfter
import AssertUntil
import AssertDuring

class Teststage1():

    def __init__(self):
        self.testaspectpassed = {}

        self.tonStageRunTime = PLCton.Ton()
        self.tonTest = PLCton.Ton()

        self.AssertTestAlwaysTrue = AssertAlways.AssertAlwaysTrue()
        self.AssertTestAlwaysFalse = AssertAlways.AssertAlwaysFalse()

        self.AssertTestTrueAfter = AssertAfter.AssertAfterTrue()
        self.AssertTestFalseAfter = AssertAfter.AssertAfterFalse()

        self.AssertTestTrueUntil = AssertUntil.AssertTrueUntil()
        self.AssertTestFalseUntil = AssertUntil.AssertFalseUntil()

        self.AssertTestTrueDuring = AssertDuring.AssertTrueDuring()
        self.AssertTestFalseDuring = AssertDuring.AssertFalseDuring()

    def call(self, startup):
        curTime = PLCtime.cycletime

        # run-time of this stage
        O_bFinished = self.tonStageRunTime.IN(not startup, 50)
        print(O_bFinished)

        # TON test
        cond = (curTime>5 and curTime<15)
        tonTestQ = self.tonTest.IN(cond, 5)
        print(tonTestQ)
        print(cond)

        # Monitor test
        self.testaspectpassed['AssertTestIsAlwaysTrue'] = self.AssertTestAlwaysTrue(tonTestQ, startup)
        self.testaspectpassed['AssertTestIsAlwaysFalse'] = self.AssertTestAlwaysFalse(tonTestQ, startup)

        self.testaspectpassed['AssertTestIsTrueAfter'] = self.AssertTestTrueAfter(tonTestQ, 11, startup)
        self.testaspectpassed['AssertTestIsFalseAfter'] = self.AssertTestFalseAfter(tonTestQ, 11, startup)

        self.testaspectpassed['AssertTestIsTrueUntil'] = self.AssertTestTrueUntil(tonTestQ, 10, startup)
        self.testaspectpassed['AssertTestIsFalseUntil'] = self.AssertTestFalseUntil(tonTestQ, 10, startup)

        self.testaspectpassed['AssertTestIsTrueDuring'] = self.AssertTestTrueDuring(tonTestQ, 11, 4, startup)
        self.testaspectpassed['AssertTestIsFalseDuring'] = self.AssertTestFalseDuring(tonTestQ, 11, 4, startup)

        # return true when finished
        return O_bFinished

        