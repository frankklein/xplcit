import PLCton

class AssertTrueDuring():
    def __init__(self):
        self.passed = False
        self.tonDelay = PLCton.Ton()
        self.tonDuration = PLCton.Ton()
    
    def __call__(self, IN, after, duration, startup):
        if startup:
            self.passed = True

        self.tonDelay.IN(not startup, after)
        self.tonDuration.IN(self.tonDelay.Q, duration)

        if self.tonDelay.Q and not self.tonDuration.Q:
            if not IN:
                self.passed = False
        
        return self.passed
    
class AssertFalseDuring():
    def __init__(self):
        self.passed = False
        self.tonDelay = PLCton.Ton()
        self.tonDuration = PLCton.Ton()

    def __call__(self, IN, after, duration, startup):
        if startup:
            self.passed = True

        self.tonDelay.IN(not startup, after)
        self.tonDuration.IN(self.tonDelay.Q, duration)

        if self.tonDelay.Q and not self.tonDuration.Q:
            if IN:
                self.passed = False
        
        return self.passed
