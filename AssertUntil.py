import PLCton

class AssertTrueUntil():
    def __init__(self):
        self.passed = False
        self.tonMonitorTime = PLCton.Ton()
    
    def __call__(self, IN, after, startup):
        if startup:
            self.passed = True

        self.tonMonitorTime.IN(not startup, after)

        if not self.tonMonitorTime.Q:
            if not IN:
                self.passed = False
        
        return self.passed
    
class AssertFalseUntil():
    def __init__(self):
        self.passed = False
        self.tonMonitorTime = PLCton.Ton()

    def __call__(self, IN, after, startup):
        if startup:
            self.passed = True

        self.tonMonitorTime.IN(not startup, after)
        
        if not self.tonMonitorTime.Q:
            if IN:
                self.passed = False
        
        return self.passed
